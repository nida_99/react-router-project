import React from 'react';
import Post from './Post';
import '../App.css'

class User extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (<>

            {
                this.props.users.map((user) => {

                    return (
                        <div key={user.id} className='user-container'>
                            <Post name={user.name} username={user.username} id={user.id} posts={this.props.posts} comments={this.props.comments} />
                        </div>)
                })
            }

        </>)
    }
}

export default User