import React from "react";
import './Cssloader.css'

class Cssloader extends React.Component {

    render() {
        return (
            <div class="lds-dual-ring"></div>
        )
    }
}

export default Cssloader;