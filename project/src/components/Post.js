import React from "react";
import '../App.css'
import Comments from "./Comments";
import { Link } from 'react-router-dom';

class Post extends React.Component {
    constructor(props) {
        super(props)
    }


    countComment = (arr) => {
        const reqArr = arr.filter((ele) => {
            if (ele.postId === this.props.id) {
                return true;
            }
        })
        return reqArr.length;
    }
    render() {
        return (
            <>

                <div className="post-container">

                    {this.props.posts.map((ele) => {

                        if (ele.userId === this.props.id) {
                            return (
                                <>
                                    <div class="completePost" key={this.props.id}>
                                        <div class="container">
                                            <img className='image' src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZlhonvIAW1NZs7kJYW1nAftLzJGa1m-zkHdz10ob4O-Pa5n9GnMCQJRmOeQXVS2aYraM&usqp=CAU" />
                                            <div className='header'>
                                                <div className="username"> {this.props.name}</div>
                                                <div > <Link to={{ pathname: `/user/${this.props.id}` }}>@{this.props.username}</Link></div>
                                            </div>
                                        </div>
                                        <div className="post">
                                            <div className="postTitle"> <Link to={{ pathname: `/post/${ele.id}` }}>{ele.title} </Link></div>
                                            <div className="postBody"> {ele.body} </div>
                                        </div>


                                        <Comments id={ele.id} comments={this.props.comments} count={this.countComment(this.props.comments)} />

                                    </div>
                                </>
                            )
                        }

                    })
                    }

                </div>


            </>
        )
    }
}

export default Post