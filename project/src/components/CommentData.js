import React from "react";


class CommentData extends React.Component {
    constructor(props) {
        super(props)

    }
    render() {
        return (
            <>
                <div className="comment-container">
                    {this.props.comments.map((ele) => {
                        if (ele.postId == this.props.id) {

                            return (<div key={ele.id} className="single-comment">
                                {
                                    <div>
                                        <div className="comment-header">
                                            <div className="bold">  {ele.name}</div>
                                            <div className="bold emailContainer">Email {ele.email}</div>
                                        </div>
                                        <p className="para">{ele.body}</p>
                                    </div>
                                }
                            </div>)

                        }
                    })}
                </div>


            </>
        )
    }
}

export default CommentData;