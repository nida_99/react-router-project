import React from "react";
import '../App.css';
import ErrorPage from "./ErrorPage";
import Cssloader from './Cssloader';

class UserData extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            status: false,
            userExist: true
        };
    };

    componentDidMount() {
        fetch("https://jsonplaceholder.typicode.com/users").then((res) => {
            return res.json();
        }).then((data) => {

            this.setState({
                users: data,
                status: true
            })

        });

    };
    render() {
        const id = this.props.match.params.id;
        if (id < 1 || id > 10) {
            this.setState({
                userExist: false
            })
        }
        return (

            <div className="App">
                {
                    (!this.state.userExist) ? <ErrorPage /> :

                        this.state.status ?

                            this.state.users.map((user) => {

                                if (user.id == id) {
                                    return (<div key={user.id} className='user-box'>
                                        <img className='user-image' src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZlhonvIAW1NZs7kJYW1nAftLzJGa1m-zkHdz10ob4O-Pa5n9GnMCQJRmOeQXVS2aYraM&usqp=CAU" />
                                        <div className='user-header'>
                                            <div className='bold-color'> <span className='bold-green'> Name </span> : {user.name} </div>
                                            <div className='bold-color'> <span className='bold-green'> Username </span> : {user.username}</div>
                                        </div>
                                        <div className='address bold-green'>  Address :  </div>
                                        <div className='addressField'>
                                            <span>{user.address.street} {user.address.suite}, {user.address.city}-{user.address.zipcode}</span>
                                        </div>
                                        <div className='contact'>
                                            <span className='bold-green'>Contact </span>: {user.phone}
                                        </div>
                                        <div className='website'>
                                            <span className='bold-green'>website </span>: {user.website}
                                        </div>

                                    </div>
                                    )
                                }
                            })
                            :
                            <div>
                                <Cssloader />
                            </div>
                }

            </div>

        )
    };
};

export default UserData;





