import React from "react";
import Cssloader from "./Cssloader";
import User from './User'

class Fetch extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            posts: [],
            comments: [],
            status: false,

        };

    };

    componentDidMount() {

        fetch("https://jsonplaceholder.typicode.com/users").then((res) => {
            return res.json();
        }).then((data) => {

            this.setState({
                users: data,

            })
            return fetch("https://jsonplaceholder.typicode.com/posts");
        }).then((res) => {
            return res.json();
        }).then((data) => {

            this.setState({
                posts: data,
            })
            return fetch("https://jsonplaceholder.typicode.com/comments");
        }).then((res) => {
            return res.json();
        }).then((data) => {

            this.setState({
                comments: data,
                status: true

            })

        });

    };
    render() {
        return (
            <div className="App">
                {
                    this.state.status ? <User users={this.state.users} posts={this.state.posts} comments={this.state.comments} />
                        :
                        <div>
                            <Cssloader />
                        </div>
                }

            </div>

        );
    };
};
export default Fetch;





