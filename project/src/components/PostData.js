import React, { Component } from 'react';
import CommentData from './CommentData';
import ErrorPage from "./ErrorPage";
import Cssloader from './Cssloader';

class PostData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: null,
            user: null,
            comments: null,
            status: false,
            postExist: true
        }

    }
    componentDidMount = () => {
        const id = this.props.match.params.id;

        if (id < 1 || id > 100) {
            this.setState({
                postExist: false
            })
        }
        let post = "";
        let user = "";


        fetch('https://jsonplaceholder.typicode.com/posts').then((res) => {
            return res.json();
        }).then((data) => {
            post = data;

            return fetch('https://jsonplaceholder.typicode.com/users')
        }).then((res) => {
            return res.json()
        }).then((data) => {
            user = data;
            return fetch('https://jsonplaceholder.typicode.com/comments')

        }).then((res) => {
            return res.json();
        }).then((data) => {
            this.setState({
                post: post,
                user: user,
                comments: data,
                status: true

            })
        })
    }
    render() {
        const id = this.props.match.params.id;
        return (
            <>
                <div className="post-container">
                    {
                        (!this.state.postExist) ? <ErrorPage /> :
                            this.state.status ?
                                this.state.post.map((ele) => {

                                    if (id == ele.id) {
                                        return (

                                            <div key={ele.id} className='posts'>
                                                <div className='only-post'>
                                                    <div className="post-title">{ele.title}</div>
                                                    <p className="post-para">{ele.body}</p>
                                                </div>
                                                <CommentData id={id} comments={this.state.comments} />
                                            </div>
                                        );
                                    };

                                })

                                :

                                <Cssloader />


                    }
                </div>

            </>
        );
    };



}

export default PostData;