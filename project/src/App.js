
import React, { Component } from 'react';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Fetch from './components/Fetch';
import PostData from './components/PostData';
import UserData from './components/UserData';
import NotFound from './components/NotFound';


class App extends Component {
  constructor(props) {
    super(props);

  }
  render() {
    return (
      <>

        <Switch>
          <Route exact path="/" component={Fetch} />
          <Route exact path="/post/:id" component={PostData} />
          <Route exact path="/user/:id" component={UserData} />

        </Switch>
      </>
    );

  }

}
<Route path="*" component={NotFound} />
export default App;










